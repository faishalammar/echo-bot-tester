Mentoria Indonesia LINE Chatbot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A LINE chatbot built for Mentoria Indonesia.

Requirements and Dependencies
-----------------------------
See ``requirements.txt`` for the full list of required Python libraries.

Installation
------------
- Clone this repository.
- Install requirements with ``pip install -r requirements.txt``.

Usage
-----
- Create a ``.env`` file in the directory where file ``manage.py`` is located.
- Variables you need to create in the ``.env`` file:
    - SECRET KEY (optional)
        If this variable is not created, then a random secret key will be generated automatically.
    - DEBUG (True or False)
        Set this variable to True if you want DEBUG to be activated.
    - LINE_CHANNEL_SECRET
    - LINE_CHANNEL_ACCESS_TOKEN
    - ALLOWED_HOSTS
        Separate each host with semi-colon (;) e.g. host1;host2;host3.
        This variable only matters if you set the DEBUG variable to False.