from django.contrib import admin

from .models import (Jenjang, Region, HariMentoring, Kafe, MataPelajaran,
                     LINESquareURL, Mentee, Mentor, Mentoring, Rumpun, SuperRegion)

admin.site.register((Jenjang, Region, HariMentoring, Kafe, MataPelajaran,
                     LINESquareURL, Mentee, Mentor, Mentoring, Rumpun, SuperRegion))
