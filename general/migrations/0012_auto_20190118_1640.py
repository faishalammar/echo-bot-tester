# Generated by Django 2.1.5 on 2019-01-18 09:40

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0011_auto_20190118_1523'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kafe',
            name='alamat',
            field=models.CharField(max_length=50, validators=[django.core.validators.MaxLengthValidator(50)]),
        ),
    ]
