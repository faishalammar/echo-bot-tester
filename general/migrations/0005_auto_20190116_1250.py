# Generated by Django 2.1.5 on 2019-01-16 05:50

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0004_auto_20190116_1200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matapelajaran',
            name='group_counter',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)]),
        ),
    ]
