from datetime import date, time

from django.db import IntegrityError
from django.test import TestCase

from .models import (Jenjang, Region, HariMentoring, Kafe, MataPelajaran,
                     LINESquareURL, Mentee, Mentor, Mentoring, Rumpun, SuperRegion)


class JenjangModelTests(TestCase):

    def test_create_jenjang_item_successfully(self):
        Jenjang(nama='Test jenjang').save()
        self.assertEqual(1, Jenjang.objects.count())

    def test_duplicate_jenjang_items_are_invalid(self):
        Jenjang(nama='a').save()
        with self.assertRaises(IntegrityError):
            Jenjang(nama='a').save()


class SuperRegionModelTests(TestCase):

    def test_create_super_region_item_successfully(self):
        SuperRegion(nama='Test super region').save()
        self.assertEqual(1, SuperRegion.objects.count())

    def test_duplicate_super_region_items_are_invalid(self):
        SuperRegion(nama='Test').save()
        with self.assertRaises(IntegrityError):
            SuperRegion(nama='Test').save()


class RegionModelTests(TestCase):

    def setUp(self):
        SuperRegion(nama='Test super region').save()

    def test_create_region_item_successfully(self):
        Region(nama='Test region',
               super_region=SuperRegion.objects.first()).save()
        self.assertEqual(1, Region.objects.count())

    def test_duplicate_region_items_are_invalid(self):
        Region(nama='a',
               super_region=SuperRegion.objects.first()).save()
        with self.assertRaises(IntegrityError):
            Region(nama='a',
                   super_region=SuperRegion.objects.first()).save()

    def test_region_item_has_correct_super_region(self):
        new_region = Region(nama='Test',
                            super_region=SuperRegion.objects.first())
        new_region.save()
        self.assertEqual('Test super region', new_region.super_region.nama)


class HariMentoringModelTests(TestCase):

    def test_create_hari_mentoring_item_successfully(self):
        HariMentoring(hari='Test hari').save()
        self.assertEqual(1, HariMentoring.objects.count())

    def test_duplicate_hari_mentoring_items_are_invalid(self):
        HariMentoring(hari='a').save()
        with self.assertRaises(IntegrityError):
            HariMentoring(hari='a').save()


class KafeModelTests(TestCase):

    def setUp(self):
        Region(nama='Region').save()
        HariMentoring(hari='Suatu hari').save()
        HariMentoring(hari='Hari lain').save()

    def test_create_kafe_item_successfully(self):
        new_kafe = Kafe(nama='Test kafe',
                        region=Region.objects.first(),
                        alamat='Alamat kafe',
                        url_google_maps='https://example.com',
                        url_gambar='https://example.com')
        new_kafe.save()
        new_kafe.hari_mentoring_tersedia.add(HariMentoring.objects.first())
        new_kafe.save()
        self.assertEqual(1, Kafe.objects.count())

    def test_kafe_item_has_correct_region_and_hari_mentoring(self):
        new_kafe = Kafe(nama='Test kafe',
                        region=Region.objects.first(),
                        alamat='Alamat kafe',
                        url_google_maps='https://example.com',
                        url_gambar='https://example.com')
        new_kafe.save()
        new_kafe.hari_mentoring_tersedia.add(HariMentoring.objects.all()[0])
        new_kafe.hari_mentoring_tersedia.add(HariMentoring.objects.all()[1])
        new_kafe.save()

        self.assertEqual('Region', new_kafe.region.nama)
        self.assertEqual(2, new_kafe.hari_mentoring_tersedia.count())
        self.assertEqual('Suatu hari',
                         new_kafe.hari_mentoring_tersedia.all()[0].hari)
        self.assertEqual('Hari lain',
                         new_kafe.hari_mentoring_tersedia.all()[1].hari)

    def test_kafe_item_has_correct_default_waktu_buka_and_waktu_tutup_value(self):
        new_kafe = Kafe(nama='Test kafe',
                        region=Region.objects.first(),
                        alamat='Alamat kafe',
                        url_google_maps='https://example.com',
                        url_gambar='https://example.com')
        new_kafe.save()
        waktu_buka_default = time(hour=10, minute=0, second=0)
        waktu_tutup_default = time(hour=23, minute=0, second=0)
        self.assertEqual(str(waktu_buka_default),
                         str(new_kafe.waktu_buka))
        self.assertEqual(str(waktu_tutup_default),
                         str(new_kafe.waktu_tutup))


class RumpunModelTests(TestCase):

    def test_create_rumpun_item_successfully(self):
        Rumpun(nama='Rumpun').save()
        self.assertEqual(1, Rumpun.objects.count())

    def test_duplicate_rumpun_items_are_invalid(self):
        Rumpun(nama='Rumpun').save()
        with self.assertRaises(IntegrityError):
            Rumpun(nama='Rumpun').save()


class MataPelajaranModelTests(TestCase):

    def setUp(self):
        Rumpun(nama='Rumpun1').save()
        Rumpun(nama='Rumpun2').save()

    def test_create_mata_pelajaran_item_successfully(self):
        MataPelajaran(nama='Mapel',
                      line_group_id='123456',
                      group_counter=1,
                      url_gambar='https//example.com').save()
        self.assertEqual(1, MataPelajaran.objects.count())

    def test_duplicate_mata_pelajaran_items_are_invalid(self):
        MataPelajaran(nama='Mapel',
                      line_group_id='123456',
                      group_counter=1,
                      url_gambar='https//example.com').save()
        with self.assertRaises(IntegrityError):
            MataPelajaran(nama='Mapel',
                          line_group_id='123456',
                          group_counter=1,
                          url_gambar='https//example.com').save()

    def test_mata_pelajaran_item_has_correct_default_group_counter_value(self):
        new_mapel = MataPelajaran(nama='Mapel', url_gambar='https//example.com')
        new_mapel.save()
        self.assertEqual(0, new_mapel.group_counter)

    def test_mata_pelajaran_item_has_correct_rumpun(self):
        new_mapel = MataPelajaran(nama='Mapel', url_gambar='www.example.com')
        new_mapel.save()
        new_mapel.rumpun.add(Rumpun.objects.all()[0])
        new_mapel.rumpun.add(Rumpun.objects.all()[1])

        self.assertEqual('Rumpun1', new_mapel.rumpun.all()[0].nama)
        self.assertEqual('Rumpun2', new_mapel.rumpun.all()[1].nama)


class LINESquareURLModelTests(TestCase):

    def setUp(self):
        MataPelajaran(nama='Test mapel',
                      url_gambar='https://example.com').save()

    def test_create_line_square_url_item_successfully(self):
        LINESquareURL(nama='Test url',
                      url='https://example.com',
                      mata_pelajaran=MataPelajaran.objects.first()).save()
        self.assertEqual(1, LINESquareURL.objects.count())


class MenteeModelTests(TestCase):

    def test_create_mentee_item_successfully(self):
        Mentee(nama='Test mentee',
               line_account_id='12345',
               email='test@mail.com').save()
        self.assertEqual(1, Mentee.objects.count())

    def test_duplicate_mentee_line_id_are_invalid(self):
        Mentee(nama='Test mentee',
               line_account_id='12345',
               email='test@mail.com').save()
        with self.assertRaises(IntegrityError):
            Mentee(nama='Test mentee',
                   line_account_id='12345',
                   email='aaa@mail.com').save()


class MentorModelTests(TestCase):

    def test_create_mentor_item_successfully(self):
        Mentor(nama='Test mentor', line_account_id='12345').save()
        self.assertEqual(1, Mentor.objects.count())

    def test_duplicate_mentor_line_id_are_invalid(self):
        Mentor(nama='Test mentor',
               line_account_id='12345').save()
        with self.assertRaises(IntegrityError):
            Mentor(nama='Test mentor',
                   line_account_id='12345').save()


class MentoringModelTests(TestCase):

    def setUp(self):
        Mentee(nama='Mentee', line_account_id='abc123', email='test@mail.com').save()
        Mentor(nama='Mentor', line_account_id='abc456').save()
        Jenjang(nama='S3').save()
        Region(nama='Jakarta Tengah').save()
        Kafe(nama='The Cafe',
             region=Region.objects.first(),
             alamat='Alamat kafe',
             url_google_maps='https://maps.com',
             url_gambar='https://image.com').save()
        MataPelajaran(nama='PPW 2',
                      line_group_id='12345',
                      url_gambar='https://image.com').save()

    def test_create_mentoring_item_successfully(self):
        Mentoring(mentee=Mentee.objects.first(),
                  mentor=Mentor.objects.first(),
                  jenjang=Jenjang.objects.first(),
                  kafe=Kafe.objects.first(),
                  mata_pelajaran=MataPelajaran.objects.first(),
                  tanggal=date(year=2019, month=1, day=16),
                  waktu_mulai=time(hour=10, minute=0, second=0),
                  waktu_selesai=time(hour=11, minute=0, second=0),
                  biaya=9999999,
                  jumlah_mentee=5,
                  is_ordered=True).save()
        self.assertEqual(1, Mentoring.objects.count())

