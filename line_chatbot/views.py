from datetime import datetime, timedelta
from time import gmtime, strftime

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseForbidden)
from django.views.decorators.csrf import csrf_exempt

from linebot import models as bot_models
from linebot.models import *
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError, LineBotApiError

from general import models as main_models
from line_chatbot.utils import (create_postback_action, create_carousel_without_image, create_carousel_column,
                                create_cafe_display, create_invoice_display)

line_bot_api = LineBotApi(settings.LINE_CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(settings.LINE_CHANNEL_SECRET)


def reply_to_user(reply_token, message):
    line_bot_api.reply_message(reply_token, message)


def create_image_carousel_for_square_url(image_url, link):
    return bot_models.ImageCarouselColumn(
            image_url=image_url,
            action=URIAction(label='Join Square', uri=link))

@handler.add(bot_models.MessageEvent, message=bot_models.TextMessage)
def handle_text_message(event):
    text = event.message.text
    print(text)
    print(event)
    profile = line_bot_api.get_profile(event.source.user_id)

    if text.lower() == 'belajar':
        # Cek dulu dia udah terdaftar di database ato belom, cek dr USERID
        if isinstance(event.source, bot_models.SourceUser):
            current_user = main_models.Mentee.objects.filter(line_account_id=profile.user_id)

            if current_user.exists():
                current_user = current_user.first()
                unfinished_transaction = main_models.Mentoring.objects.filter(mentee=current_user). \
                    filter(is_ordered=False)
                if unfinished_transaction.exists():
                    reply_to_user(event.reply_token, [
                        bot_models.TextSendMessage(
                            text=current_user.nama + ", kamu punya transaksi yang tertunda, lengkapi pesanan mu "
                                                     "sebelumnya. ketik '.batal' untuk menghapus pesanan sebelumnya"),
                    ])

                else:
                    main_models.Mentoring.objects.create(mentee=current_user)
                    rumpun = []
                    for i in main_models.Rumpun.objects.all():
                        rumpun.append(create_postback_action(i.nama, "rumpun " + i.nama))
                    rumpun_carousel = bot_models.TemplateSendMessage(  # template data matpel
                        alt_text='Carousel template',
                        template=create_carousel_without_image("Rumpun pelajaran",
                                                               "Pilih rumpun pelajaran",
                                                               rumpun)
                    )
                    reply_to_user(event.reply_token, [
                        bot_models.TextSendMessage(
                            text="Halo " + current_user.nama + ", kamu mau belajar mata pelajaran apa?"),
                        rumpun_carousel
                    ])

            else:
                reply_to_user(event.reply_token, [
                    bot_models.TextSendMessage(
                        text="Halo " + profile.display_name + ",\nSebelum nya kami membutuhkan nama beserta "
                                                              "email kamu nih, balas dengan \n"
                                                              "/daftar <Nama Lengkap> <Email>""."
                                                              " \nContoh, /daftar Budi Budian@gmail.com"),
                ])

        else:
            reply_to_user(event.reply_token,
                          bot_models.TextSendMessage(text="Bot can't use profile API without user ID"))

    # Pendaftaran
    elif text.lower() == '.batal':
        mentee = main_models.Mentee.objects.get(line_account_id=profile.user_id)
        main_models.Mentoring.objects.filter(mentee=mentee).get(is_ordered=False).delete()
        reply_to_user(event.reply_token,
                      bot_models.TextSendMessage(text='Transaksi berhasil dibatalkan.'))

    elif text.lower() == 'reset':
        main_models.Mentee.objects.get(line_account_id=profile.user_id).delete()
        reply_to_user(event.reply_token,
                      bot_models.TextSendMessage(text="Menghapus memori.."))

    elif text.split(" ")[0] == '/daftar':
        current_user = main_models.Mentee.objects.filter(line_account_id=profile.user_id)

        if current_user.exists():
            current_user = current_user.first()
            reply_to_user(event.reply_token,
                          bot_models.TextSendMessage(
                              text=current_user.nama + ', kamu telah terdaftar. '
                                                       'Ketik "belajar" untuk melakukan pemesanan'))

        else:
            user = " ".join(text.split(" ")[1:-1])
            mail = text.split(" ")[-1]
            user_id = profile.user_id
            try:
                validate_email(mail)
                main_models.Mentee.objects.create(nama=user, line_account_id=user_id, email=mail)
                reply_to_user(
                    event.reply_token, [
                        bot_models.TextSendMessage(
                            text="Selamat " + user + " kamu telah terdaftar sebagai pengguna Mentoria.id"),
                        bot_models.TextSendMessage(text=user + ', ketik "belajar" untuk memulai pemesanan')
                    ])
            except ValidationError:
                reply_to_user(
                    event.reply_token,
                    bot_models.TextSendMessage(text=mail + " bukan Email yang valid. Silahkan coba lagi."))

    # =============Create model user===============
    elif text.lower().split(" ")[0] == '/tambah':
        nama_mentor = text.split(" ")[1:-1]
        id_mentor = profile.user_id
        current_mentor = main_models.Mentor.objects.filter(line_account_id=id_mentor)

        if current_mentor.exists():
            current_mentor = current_mentor.first()
            reply_to_user(event.reply_token,
                          bot_models.TextSendMessage(
                              text=nama_mentor + ' sudah pernah didaftarkan'))

        else:
            main_models.Mentee.objects.create(nama=nama_mentor, line_account_id=id_mentor)
            reply_to_user(
                event.reply_token, [
                    bot_models.TextSendMessage(
                        text="Selamat " + nama_mentor + ", kamu telah terdaftar sebagai mentor di Mentoria.id"),

                ])

    # buat left grup
    elif text.lower() == 'bye':
        if isinstance(event.source, bot_models.SourceGroup):
            reply_to_user(
                event.reply_token, bot_models.TextSendMessage(
                    text='Dadah dikick ama ' + line_bot_api.get_profile(event.source.user_id).display_name))
            line_bot_api.leave_group(event.source.group_id)
        elif isinstance(event.source, bot_models.SourceRoom):
            reply_to_user(
                event.reply_token, bot_models.TextSendMessage(
                    text='Dadah dikick ama ' + line_bot_api.get_profile(event.source.user_id).display_name))
            line_bot_api.leave_room(event.source.room_id)
        else:
            reply_to_user(event.reply_token,
                          bot_models.TextSendMessage(text="Bot can't leave from 1:1 chat"))

    elif text.lower() == '.help':
        # TODO Buat fungsionalitas .help
        help_button = bot_models.ButtonsTemplate(title="Butuh bantuan?", text="Temukan solusinya",
                                                 actions=[
                                                     bot_models.PostbackAction(label='Cara pemesanan',
                                                                               data='Help cara'),
                                                     bot_models.PostbackAction(label='Setelah pemesanan',
                                                                               data='Help setelah'),
                                                 ])
        template_message = bot_models.TemplateSendMessage(alt_text='Confirm alt text', template=help_button)
        reply_to_user(event.reply_token, template_message)

    else:
        reply_to_user(event.reply_token, bot_models.TextSendMessage(text="Anda perlu bantuan? balas "".help"" "))


@handler.add(bot_models.PostbackEvent)
def handle_postback(event):
    postback_data = event.postback.data
    print(postback_data)
    profile = line_bot_api.get_profile(event.source.user_id)
    mentee = main_models.Mentee.objects.get(line_account_id=profile.user_id)
    try:
        edited_mentoring = main_models.Mentoring.objects.filter(mentee=mentee).get(is_ordered=False)

        if postback_data.split(" ")[0] == 'rumpun':
            mapel_carousel_columns = []
            selected_rumpun = main_models.Rumpun.objects.filter(nama=event.postback.data.split(" ")[1]).first()
            for i in main_models.MataPelajaran.objects.filter(rumpun=selected_rumpun):
                mapel_carousel_columns.append(
                    create_carousel_column(i.url_gambar, i.nama, i.rumpun.all()[0].nama,
                                           [create_postback_action("Pilih", "Matpel " + i.nama)])
                )
            mapel_carousel = bot_models.TemplateSendMessage(  # template data matpel
                alt_text='Pilih mata pelajaran',
                template=bot_models.CarouselTemplate(
                    columns=mapel_carousel_columns
                )
            )
            reply_to_user(event.reply_token, mapel_carousel)

        elif postback_data.split(" ")[0] == 'Matpel':
            mapel_name = postback_data.split(" ")[1]  # Dapetin data matpel yg udh dipilih sebelumnya
            edited_mentoring.mata_pelajaran = main_models.MataPelajaran.objects.filter(nama=mapel_name).first()
            edited_mentoring.save()

            grade_columns = []
            for i in main_models.Jenjang.objects.all():
                grade_columns.append(create_postback_action(i.nama, i.nama))

            jenjang_carousel = bot_models.TemplateSendMessage(  # template data matpel
                alt_text='Carousel template',
                template=create_carousel_without_image(
                    "Jenjang Pelajaran",
                    "Pilih jenjang kamu",
                    grade_columns)
            )
            reply_to_user(
                event.reply_token, [
                    bot_models.TextSendMessage(text='Kamu memilih pelajaran ' + mapel_name),
                    bot_models.TextSendMessage(text='Selanjutnya, pilih jenjang kamu ya'),
                    jenjang_carousel
                ])

        elif postback_data.split(" ")[0] == 'Kelas' or postback_data == 'SBMPTN':
            template = bot_models.ImageCarouselTemplate(columns=[
                bot_models.ImageCarouselColumn(
                    image_url='https://i.ibb.co/NZZw9YF/adsdsdsad.png',
                    action=bot_models.DatetimePickerAction(label='Click here',
                                                           data='datetime_postback',
                                                           mode='datetime')),
            ])
            date_template = bot_models.TemplateSendMessage(alt_text='ImageCarousel alt text', template=template)
            edited_mentoring.jenjang = main_models.Jenjang.objects.filter(nama=postback_data).first()
            edited_mentoring.save()
            reply_to_user(
                event.reply_token, [bot_models.TextSendMessage(text='Kamu memilih jenjang ' + postback_data),
                                    bot_models.TextSendMessage(text='Yuk pilih waktu belajar kamu'), date_template
                                    ]
            )

        elif postback_data == 'datetime_postback':
            # ===Format bikin datetime===
            datetime_format = strftime("%Y-%m-%d %H:%M:%S", gmtime()).split(" ")
            print(datetime_format)
            user_choice = event.postback.params['datetime'].split('T')
            print(user_choice)
            current_date = datetime_format[0].split("-")
            print(current_date)
            current_time = datetime_format[1].split(":")
            print(current_time)
            selected_date = user_choice[0].split("-")
            print(selected_date)
            selected_time = user_choice[1].split(":")
            print(selected_time)

            TIME_DIFFERENCE = 7
            current_datetime = datetime(year=int(current_date[0]), month=int(current_date[1]), day=int(current_date[2]),
                                        hour=int(current_time[0]), minute=int(current_time[1]))
            current_datetime = current_datetime + timedelta(hours=TIME_DIFFERENCE)

            selected_datetime = datetime(year=int(selected_date[0]), month=int(selected_date[1]),
                                         day=int(selected_date[2]), hour=int(selected_time[0]),
                                         minute=int(selected_time[1]))
            print(current_datetime)
            print(selected_datetime)

            date_template = bot_models.ImageCarouselTemplate(
                columns=[
                    bot_models.ImageCarouselColumn(image_url='https://i.ibb.co/NZZw9YF/adsdsdsad.png',
                                                   action=bot_models.DatetimePickerAction(label='Click here',
                                                                                          data='datetime_postback',
                                                                                          mode='datetime'))
                ])
            template_message = bot_models.TemplateSendMessage(alt_text='ImageCarousel alt text', template=date_template)

            if current_datetime > selected_datetime:
                reply_to_user(event.reply_token, [
                    bot_models.TextSendMessage(text="Waktu yang anda pilih sudah terlewat, silahkan pilih lagi"),
                    template_message
                ])

            else:
                if (selected_datetime - current_datetime) > timedelta(hours=12):
                    reply_to_user(event.reply_token, [
                        bot_models.TextSendMessage(text="Waktu pemesanan terlalu dini, pastikan jam pemesanan mu "
                                                        "kurang dari 12 jam dari sekarang"),
                        template_message
                    ])

                else:
                    edited_mentoring.tanggal = user_choice[0]
                    edited_mentoring.waktu_mulai = user_choice[1]
                    edited_mentoring.save()
                    template = bot_models.ImageCarouselTemplate(columns=[
                        bot_models.ImageCarouselColumn(image_url='https://i.ibb.co/NZZw9YF/adsdsdsad.png',
                                                       action=bot_models.DatetimePickerAction(
                                                           label='Click here',
                                                           data='Kelar ' + selected_datetime.strftime("%Y-%m-%d %H:%M"),
                                                           mode='time')),
                    ])
                    date_template = bot_models.TemplateSendMessage(alt_text='ImageCarousel alt text', template=template)
                    #
                    reply_to_user(
                        event.reply_token, [
                            bot_models.TextSendMessage(text='Kamu memilih tanggal ' + user_choice[0] +
                                                            " pada jam " + user_choice[1]),
                            bot_models.TextSendMessage(text='Pilih waktu selesai pembelajarannya yuk'),
                            date_template
                        ])

        elif postback_data.split(" ")[0] == "Kelar":
            postback_data_split = postback_data.split(" ")
            user_choice = event.postback.params['time']
            print(user_choice)
            start_date = postback_data_split[1].split("-")
            start_time = postback_data_split[2].split(":")
            selected_time = user_choice.split(":")
            session_start = datetime(year=int(start_date[0]), month=int(start_date[1]), day=int(start_date[2]),
                                     hour=int(start_time[0]), minute=int(start_time[1]))
            session_end = datetime(year=int(start_date[0]), month=int(start_date[1]), day=int(start_date[2]),
                                   hour=int(selected_time[0]), minute=int(selected_time[1]))

            if session_end <= session_start:
                reply_to_user(event.reply_token,
                              [bot_models.TextSendMessage(text='Maaf, waktu yang dipilih tidak valid')])

            else:
                edited_mentoring.waktu_selesai = session_end
                edited_mentoring.save()
                # Temporary hardcode until further decision is made
                # TODO integrasi dengan database (relasi Region dengan Kafe)
                daerah_carousel = bot_models.TemplateSendMessage(  # Template daftar daerah
                    alt_text='Daerah',
                    template=bot_models.CarouselTemplate(
                        columns=[
                            create_carousel_column('https://i.ibb.co/7kBQmyT/cerita-cafe1.jpg',
                                                   'Jakarta Selatan', 'Pilih daerah berikut',
                                                   [
                                                       create_postback_action("Pasar Minggu", "Daerah Pasar Minggu"),
                                                       create_postback_action("Kemang", "Daerah Kemang"),
                                                       create_postback_action("Tebet", "Daerah Tebet")
                                                   ]),
                            create_carousel_column('https://i.ibb.co/nM0Z9bh/Pause.jpg',
                                                   'Jakarta TImur', 'Pilih daerah berikut',
                                                   [
                                                       create_postback_action("Kelapa Gading", "Daerah Kelapa Gading"),
                                                       create_postback_action("Klender", "Daerah Klender"),
                                                       create_postback_action("Pondok Bambu", "Daerah Pondok Bambu")
                                                   ]),
                        ]
                    )
                )
                reply_to_user(event.reply_token, [
                    bot_models.TextSendMessage(text='Pilih daerah cafe yang kamu inginkan'),
                    daerah_carousel
                ])

        elif postback_data.split(" ")[0] == 'Daerah':
            all_cafes = main_models.Kafe.objects.all()
            cafe_columns = []
            for cafe in all_cafes:
                cafe_times = cafe.waktu_buka.strftime("%H:%M") + " - " + cafe.waktu_tutup.strftime("%H:%M")
                cafe_columns.append(
                    create_cafe_display(cafe.url_gambar,
                                        cafe.nama,
                                        cafe.alamat,
                                        cafe_times,
                                        cafe.url_google_maps)
                )
            if isinstance(event.source, bot_models.SourceUser):
                cafes_carousel = bot_models.CarouselContainer(contents=cafe_columns)
                reply_message = bot_models.FlexSendMessage(alt_text="Mau belajar di mana?",
                                                           contents=cafes_carousel)
                reply_to_user(event.reply_token, reply_message)

        elif postback_data.split("-")[0] == 'Jumlah':
            selected_cafe_name = postback_data.split("-")[1]
            selected_cafe = main_models.Kafe.objects.get(nama=selected_cafe_name)
            edited_mentoring.kafe = selected_cafe
            edited_mentoring.save()

            MENTEE_NUMBER_SELECTIONS = {'Privat': 'print-1', '2 Orang': 'print-2',
                                        '3 Orang': 'print-3', '4 Orang': 'print-4',
                                        '5 Orang': 'print-5'}
            mentee_number_quick_replies = bot_models.QuickReply(
                items=[bot_models.QuickReplyButton(
                    action=bot_models.PostbackAction(label=key, data=value)
                ) for key, value in MENTEE_NUMBER_SELECTIONS.items()]
            )
            reply_to_user(event.reply_token,
                          bot_models.TextSendMessage(
                              text='Jumlah orangnya berapa?',
                              quick_reply=mentee_number_quick_replies
                          ))

        elif postback_data.split("-")[0] == 'print':
            mentee_number = int(postback_data.split("-")[1])
            edited_mentoring.jumlah_mentee = mentee_number
            edited_mentoring.save()

            display_invoice_button = bot_models.BubbleContainer(
                direction='ltr',
                body=bot_models.BoxComponent(
                    layout='vertical',
                    contents=[bot_models.ButtonComponent(
                        style='primary', height='md', color='#1ED056',
                        action=bot_models.PostbackAction(label='Print Invoice', data="Fix")
                    )]
                )
            )
            reply_message = bot_models.FlexSendMessage(
                alt_text="Print Invoice", contents=display_invoice_button
            )
            reply_to_user(event.reply_token, reply_message)

        elif postback_data == "Fix":
            edited_mentoring_waktu_mulai_formatted = edited_mentoring. \
                waktu_mulai.strftime("%H:%M")
            edited_mentoring_waktu_selesai_formatted = edited_mentoring. \
                waktu_selesai.strftime("%H:%M")
            invoice = create_invoice_display(
                matpel=edited_mentoring.mata_pelajaran.nama,
                nama_mentee=edited_mentoring.mentee.nama,
                tempat=edited_mentoring.kafe.nama,
                tanggal=edited_mentoring.tanggal.strftime("%A, %d %B %Y"),
                waktu=edited_mentoring_waktu_mulai_formatted + " - " + edited_mentoring_waktu_selesai_formatted,
                grade=edited_mentoring.jenjang.nama,
                jumlah_mentee=str(edited_mentoring.jumlah_mentee),
                harga=str(150000)
                # Temporary hardcode until a new agreement is made
            )
            confirm_template = bot_models.ConfirmTemplate(
                text='Konfirmasi Data?',
                actions=[
                    create_postback_action("Iya", "confirm Yes"),
                    create_postback_action("Tidak", "confirm No")
                ]
            )
            confirm_flex = bot_models.FlexSendMessage(alt_text="Confirm flex", contents=invoice)
            confirm_message = bot_models.TemplateSendMessage(alt_text="Confirm text", template=confirm_template)
            reply_to_user(event.reply_token, [confirm_flex, confirm_message])

        elif postback_data == "confirm Yes":

            # Temporary hardcode until further decision is made



            matpel_selected = edited_mentoring.mata_pelajaran
            matpel_selected.group_counter+=1
            matpel_selected.save()
            arr_channel = main_models.LINESquareURL.objects.filter(mata_pelajaran = matpel_selected)
            jumlah_channel = len(arr_channel)
            print('jumlah channel = ' + str(jumlah_channel))
            idx = matpel_selected.group_counter % jumlah_channel
            square = arr_channel[idx].url

            edited_mentoring_waktu_mulai_formatted = edited_mentoring. \
                waktu_mulai.strftime("%H:%M")
            edited_mentoring_waktu_selesai_formatted = edited_mentoring. \
                waktu_selesai.strftime("%H:%M")
            invoice = create_invoice_display(
                matpel=edited_mentoring.mata_pelajaran.nama,
                nama_mentee=edited_mentoring.mentee.nama,
                tempat=edited_mentoring.kafe.nama,
                tanggal=edited_mentoring.tanggal.strftime("%A, %d %B %Y"),
                waktu=edited_mentoring_waktu_mulai_formatted + " - " + edited_mentoring_waktu_selesai_formatted,
                grade=edited_mentoring.jenjang.nama,
                jumlah_mentee=str(edited_mentoring.jumlah_mentee),
                harga=str(150000),
                link_square= square)

            line_bot_api.push_message(to='Ce68b4d2699679de6a73dba84ffd855d6',messages=[bot_models.FlexSendMessage(alt_text="[ NEW ORDER ]", contents=invoice)])

            # edited_mentoring.is_ordered = True
            # edited_mentoring.biaya = 150.000  # Temporary hardcode until further decision is made
            # edited_mentoring.save()
            col = create_image_carousel_for_square_url(image_url='https://i.ibb.co/5vSCmfJ/Be-Funky-photo-5.jpg',
                                                       link=square)

            image_carousel_template = bot_models.ImageCarouselTemplate(columns=[col])

            send_image_template = bot_models.TemplateSendMessage(alt_text='Line Square Invitation', template=image_carousel_template)

            reply_to_user(
                event.reply_token,[send_image_template]
            )

            # templates = TemplateSendMessage(ImageCarouselTemplate(columns=[col]))
            # reply_to_user(event.reply_token,[templates])

            # reply_to_user(event.reply_token,
            #               message=bot_models.TextSendMessage(
            #                   text="Terima kasih {}, untuk melakukan komunikasi lebih "
            #                        "lanjut dengan mentor yang kamu dapat,"
            #                        " silahkan masuk ke LINE Square di link berikut: line.com/sqr249=12".
            #                       format(profile.display_name)
            #               ))


        # Temporary solution until further decision is made
        elif postback_data == "confirm No":
            edited_mentoring.delete()
            reply_to_user(event.reply_token,
                          bot_models.TextSendMessage(
                              text='Transaksi kamu sudah dihapus. Silahkan jawab "belajar" untuk mengulang transaksi.'
                          ))

    except main_models.Mentoring.DoesNotExist:
        reply_to_user(event.reply_token, bot_models.TextSendMessage(
            text='Kamu belum memulai transaksi. Balas "belajar" atau tap Rich Picture untuk'
                 'memulai transaksi.'
        ))


@csrf_exempt
def callback(request):
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')

        try:
            handler.handle(body, signature)
        except InvalidSignatureError:
            return HttpResponseForbidden()
        except LineBotApiError:
            return HttpResponseBadRequest()
        return HttpResponse()
    else:
        return HttpResponseBadRequest()
