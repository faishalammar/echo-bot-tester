from django.urls import path, re_path

from .views import callback

app_name = 'line_chatbot'
urlpatterns = [
    path('callback/', callback, name='index'),
]