# Template buat postback
from linebot.models import (PostbackAction, CarouselColumn, ButtonsTemplate, ImageComponent, BoxComponent,
                            TextComponent, SpacerComponent, ButtonComponent, SeparatorComponent, URIAction,
                            BubbleContainer)


def create_postback_action(labels, data):
    return PostbackAction(label=labels, data=data)


# Template buat carousel dengan image
def create_carousel_column(image_url, titles, texts, action):
    return CarouselColumn(thumbnail_image_url=image_url, title=titles,
                          text=texts, actions=action)  # Action berisi postback


def create_carousel_without_image(titles, texts, action):
    return ButtonsTemplate(title=titles, text=texts, actions=action)  # Action berisi postback


# Template buat bikin tampilan cafe

def create_cafe_display(image_url, name, adress, time, gmaps_url):
    bubble_hero = ImageComponent(url=image_url, size='full', aspect_ratio='20:13', aspect_mode='cover')
    bubble_body = BoxComponent(layout='vertical',
                               contents=[
                                   # Title
                                   TextComponent(text=name, weight='bold', size='xl'),
                                   # Info
                                   BoxComponent(layout='vertical', margin='lg', spacing='sm',
                                                contents=[
                                                    BoxComponent(layout='baseline', spacing='sm',
                                                                 contents=[
                                                                    TextComponent(
                                                                        text='Place', color='#aaaaaa',
                                                                        size='sm', flex=1),
                                                                    TextComponent(
                                                                        text=adress, wrap=True,
                                                                        color='#666666', size='sm', flex=5)
                                                                 ]),
                                                    BoxComponent(layout='baseline', spacing='sm',
                                                                 contents=[
                                                                     TextComponent(
                                                                         text='Time', color='#aaaaaa',
                                                                         size='sm', flex=1),
                                                                     TextComponent(
                                                                         text=time, wrap=True,
                                                                         color='#666666', size='sm', flex=5)
                                                                 ])
                                                ])
                               ])
    bubble_footer = BoxComponent(layout='vertical', spacing='sm',
                                 contents=[
                                     # callAction, separator, websiteAction
                                     SpacerComponent(size='sm'),
                                     # callAction
                                     ButtonComponent(style='primary', height='sm', color='#1ED056',
                                                     action=PostbackAction(label='Choose', data="Jumlah-" + name)),
                                     # separator
                                     SeparatorComponent(),
                                     # websiteAction
                                     ButtonComponent(style='link', height='sm',
                                                     action=URIAction(label='Location', uri=gmaps_url))
                                 ])

    return BubbleContainer(
        direction='ltr',
        hero=bubble_hero,
        body=bubble_body,
        footer=bubble_footer)


def create_invoice_display(matpel, nama_mentee, tempat, tanggal, waktu, grade, jumlah_mentee, harga, link_square=None):
    footer = None

    if link_square!=None:
        footer = BoxComponent(layout='vertical',spacing='xl',
                            contents=[
                                ButtonComponent(
                                    style='link',
                                    height='sm',
                                    action=URIAction(label='Ambil - Join Channel',uri=link_square)
                                )
                            ])


    return BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            contents=[
                TextComponent(
                    text='RINGKASAN',
                    weight='bold',
                    color='#1DB446',
                    size='sm'
                ),
                TextComponent(
                    text=matpel,
                    weight='bold',
                    size='xxl',
                    margin='md'
                ),
                TextComponent(
                    text=nama_mentee,
                    size='xs',
                    color='#aaaaaa',
                    wrap=True
                ),
                SeparatorComponent(
                    margin='xxl',
                    color='#aaaaaa'
                ),
                BoxComponent(
                    layout='vertical',
                    margin='xxl',
                    spacing='sm',
                    contents=[
                        BoxComponent(
                            layout='horizontal',
                            contents=[
                                TextComponent(
                                    text="Tempat",
                                    size='sm',
                                    color='#555555',
                                    flex=0
                                ),
                                TextComponent(
                                    text=tempat,
                                    size='sm',
                                    color="#111111",
                                    align='end'
                                )
                            ]
                        ),
                        BoxComponent(
                            layout='horizontal',
                            contents=[
                                TextComponent(
                                    text="Tanggal",
                                    size='sm',
                                    color='#555555',
                                    flex=0
                                ),
                                TextComponent(
                                    text=tanggal,
                                    size='sm',
                                    color="#111111",
                                    align='end'
                                )
                            ]
                        ),
                        BoxComponent(
                            layout='horizontal',
                            contents=[
                                TextComponent(
                                    text="Waktu",
                                    size='sm',
                                    color='#555555',
                                    flex=0
                                ),
                                TextComponent(
                                    text=waktu,
                                    size='sm',
                                    color="#111111",
                                    align='end'
                                )
                            ]
                        ),
                        BoxComponent(
                            layout='horizontal',
                            contents=[
                                TextComponent(
                                    text="Grade",
                                    size='sm',
                                    color='#555555',
                                    flex=0
                                ),
                                TextComponent(
                                    text=grade,
                                    size='sm',
                                    color="#111111",
                                    align='end'
                                )
                            ]
                        ),
                        BoxComponent(
                            layout='horizontal',
                            contents=[
                                TextComponent(
                                    text="Jumlah",
                                    size='sm',
                                    color='#555555',
                                    flex=0
                                ),
                                TextComponent(
                                    text=jumlah_mentee,
                                    size='sm',
                                    color="#111111",
                                    align='end'
                                )
                            ]
                        ),
                        SeparatorComponent(
                            margin='xxl',
                            color='#aaaaaa'
                        ),
                        BoxComponent(
                            layout='horizontal',
                            contents=[
                                TextComponent(
                                    text="Harga",
                                    size='sm',
                                    color='#555555',
                                    flex=0
                                ),
                                TextComponent(
                                    text=harga,
                                    size='sm',
                                    color="#111111",
                                    align='end'
                                )
                            ]
                        ),
                    ]
                )
            ]
        ),
        footer=footer
    )
